package com.example;

public class Client {

    private final DBService dbService;
    private final HttpService httpService;

    public Client() {
        this.dbService = new DBService();
        this.httpService = new HttpService();
    }

    public void GetResources(String url) {
        System.out.println("Making some request....");
        System.out.println(httpService.get(url));
        System.out.println(dbService.GetOutSomething());
        System.out.println("Done");
    }
}
