# Beispiel Aufgabe zur Dependency Injections

Baue das Java Programm so um, dass es Dependency Injection nutzt:

1. Setze DI für den DBService um
    - Der DBService soll von außen konfigurierbar sein
2. Bau den Client so um, dass der DBService und HttpService als Dependency reingereicht werden. 



PS: Die Lösung mit dem Constructor Injection findet ihr in der develop branch xD
